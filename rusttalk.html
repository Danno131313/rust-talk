<!DOCTYPE html>
<html>
  <head>
    <title>RustTalk</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      h1 {
        margin-bottom: 15px;
      }
      li {
        margin-bottom: 15px;
      }
      ul {
        margin-top: 10px;
      }
      .bold {
        font-weight: bold;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; font-size: 1.1em;}
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle

# It's Time to Oxidize
### A presentation on the Rust programming language and why you should care

---

# About

* General-purpose programming language (focus on systems programming)
* Open-source, sponsored by Mozilla
* Pursues the holy trifecta: Safety, Concurrency, and Speed
* High level syntax and expressiveness, low level control and performance
* Procedural programming with aspects of O.O.P. and F.P.
* No garbage collection or manual memory management
* Fun, modern, and productive!
  * Released in 2015
  * \#1 most loved language in 2016, 2017, and 2018 on Stack Overflow

---

# Who Even Uses Rust?
### Lots of companies do!

* Mozilla (Servo browser engine for Firefox)
* Dropbox
* npm
* Postmates
* Discord
* Canonical
* Coursera
* and many more!

???
Starting to be adopted by companies more and more as more people learn about how awesome it is!

---

# Why it's cool
* Well designed:
  * Feature stabilization follows a strict process
  * Backwards compatibility is emphasized
  * Learned from other languages' mistakes
* Awesome, helpful error messages
* Cargo - all-in-one project management, code formatting, and binary installation
* Performance
  * Fastest JSON serialization/deserialization library (serde)
  * Fastest asynchronous web framework (actix_web)
  * Fastest grepping program (ripgrep)

???
Focused on sticking to well-defined standards.

Error messages show you the exact code where problems happened, and how to fix them (most of the time)

Rust has insane performance: As fast as C in many aspects, and sometimes faster.

Cargo controls what libraries your project uses, manages building and running the project, and even has built-in tools for autoformatting your code to the Rust standard, fixing common errors, making sure your code follows common idioms of the language, and can even publish your package to the internet. Can also be used to install Rust programs like ripgrep.

---

class: center, middle

# Language Features
### What makes Rust so enjoyable to use

---

# Structs and Enums

Rust supports algebraic datatypes, taken from functional programming.

```rust
// Regular, named-property struct
struct Turn {
    action: Action,
    player_name: String,
}

struct Point(i32, i32); // Tuple struct

struct Empty; // Empty struct

enum Action {
    Quit,                       // Regular enum variant
    Move { x: i32, y: i32 },    // Anonymous struct
    Say(String),                // singleton tuple variant
    ChangeColor(i32, i32, i32), // three item tuple variant
}
```

???
Rust allows for using normal named-field structs, tuple-like structs, and empty (or 'unit') structs, and allows enums to be built up using all the different types.

---

# Impl block

Methods can be defined for types in a `impl` block:
```rust
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Point {
        Point {
            x,
            y,
        }
    }

    pub fn move(&mut self, x: i32, y: i32) { // Ignore &mut for now
        self.x += x;
        self.y += y;
    }
}
```

???
Implementing methods for a struct or enum is done in an impl block.

Methods that don't explicitly take 'self' as an argument are called "associated functions" and can be called without instantiating the type. To do anything with an actual instance of the type you must include self as an argument.

---

# Pattern matching

Pattern matching is everywhere and allows for some convenient behavior:
```rust
struct Person {
    age: u32,
    name: String
}

let me = Person {
    name: "Danny".to_string(),
    age: 23,
};

// 'age' and 'name' matched and created from the return value
let Person { age, name } = make_person();
```

???
In this example, the return value of make_person() is destructured in the let binding, allowing you to create variables from the inner properties and disregard the wrapping struct.

---

Pattern matching cont.
```rust
enum Option<T> { // 'T' is any type
    Some(T),
    None,
}

// matching in if statements!
if let Some(val) = some_option {
    do_something(val);
}

match returns_option() {
    Some(_) => do_something(), // '_' = don't care about the value
    None => do_something_else(),
    // _ => do_yet_another_thing(), // Instead of using None
};
```

???
This example uses the standard library's Option type. The same pattern matching with let from the previous slide can also be used in if statements! 

Also, the match operator (like other languages' switch case statements) can be used to match possible patterns of a value.

The underscore means we don't care about the value, and can even be used as one of the match arms as a "anything else" pattern.

---

# Error handling

Rust does not have exceptions. All functions that may fail return either a `Result<T, E>` or an `Option<T>`
```rust
enum Result<T, E> { // T is a value type, E is an Error
    Ok(T),
    Err(E),
}

enum Option<T> {
    Some(T),
    None,
}
```
Some errors you don't want to be handled, in which case you call panic!() to terminate the program and get a stack trace printed to the terminal:
```rust
// .unwrap() attempts to retrieve the inner value in Ok(val),
// and calls panic!() if it's an Err(error)

let res = some_result.unwrap(); // panic!()
```

???


---

# Macros

Rust has macro functions, called with a '!':
```rust
// print formatting with any number of arguments
println!("{}, {}: age {}", last_name, first_name, age);

// easy JSON serialization
let j = json!({
    id: 1,
    first_name,
    age,
});
```

Rust also has procedural macros for deriving traits and metaprogramming:
```rust
#[derive(Serialize, Deserialize)]
struct Person {
    name: String,
    age: i32,
}
```

---

# Traits
Traits are like C# interfaces but more powerful. Arguably the most powerful tool in Rust, they allow for a very high level of abstraction.

```rust
impl Iterator for T {
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        ...
    }
}

// T gets these methods for free and many more!
let iter: Vec<T> = T.filter(|val| val != other_val)
                    .zip(other_iter)
                    .collect();
```
* Lazily evaluated, optimized during compilation

---

Trait objects can be statically or dynamically dispatched from functions:
```rust
use std::io::{Read, Write};
use std::fs::File;

// static dispatch
fn get_readable_file() -> impl Read {
    let file: File = File::open("/path").unwrap();
    return file; // optional return statement
}

let buffer = String::new();
get_readable_file().read(&mut buffer);

// dynamic dispatch
fn get_writable_file() -> Box<Write> {
    let file: File = File::create("/path").unwrap();
    Box::new(file) // optional return statement
}

get_writeable_file().write("message");
```
Wrapping with a `Box<T>` explicitly allocates a value to the heap.

---

# Closures

Rust closures are different than most other languages' closures:
* Not heap allocated (can be `Box`ed)
* No overhead
* Syntax sugar for traits: Fn, FnMut, FnOnce
  * Regular functions implement the same traits!

```rust
items.filter(|item| item.value < 100)
     .for_each(|item| {
         item.value = 100;
         put_into_db(item.clone());
     });
```

---

# Zero-cost abstraction

"What you don't use, you don't pay for, and what you do use, you couldn't hand-code any better." - C++

* Generics are compiled away, like you wrote specialized definitions for each type
  * Statically dispatched
* Closures are just as fast as regular functions
* Wrapper types are compiled away
* Iterators compile to the same code as regular loops

```rust
// diesel, a Rust ORM for SQL databases
let users_and_groups = users
                        .filter(user_id.eq(user.id))
                        .inner_join(groups::table)
                        .load::<(User, Group)>(&conn)
                        .unwrap();
```
---
class: center, middle

# But how is all of this possible?
### No garbage collection??
### Low memory footprint?!?

---

# Ownership and Moves

Every created value and reference has a lifetime: they exist while in scope, and are dropped from memory when out of scope (the scope 'owns' the value).
* Values can be moved to different scopes

```rust
fn drop(val: T) {} // Takes value directly (move), does nothing

fn main() {
    let mut s = String::new(); // declares mutable string

    drop(s);
    drop(s); // ERROR! Cannot use s after it's been moved

    let s1 = String::new();
    let s2 = s1; // moves s1 into s2

    println!("{}", s1); // ERROR! Cannot use s1 after move
    println!("{}", s2); // prints ""
}
```

---

# References (`&`) and the Borrow Checker

References are a way to avoid moves, but come with some rules:
* Cannot outlive lifetime of what it references
* Value can have any number of immutable references (`&`) .bold[OR] one mutable reference (`&mut T`) at a time 

Methods can take `self`, `&self`, or `&mut self` depending on what you want to do.
* `self` moves the whole value into the method (destroyed unless moved or returned)
* `&self` allows reading of interior properties, no mutation (ways around this)
* `&mut self` allows mutation of interior properties

The borrow checker enforces these rules at compile time.
* Love/hate relationship
* Fearless concurrency

---

```rust
let mut s = String::new(); // must declare mutable
s.push("Hello,"); // method takes &mut self
s.push(" World!"); // &mut self, still works

let i = &mut s;
let t = &mut s; // ERROR! cannot reference mutably twice
```
```rust
let s = String::new();
let i = &s; // immutable reference
let t = &s; // another, works
let u = &mut s; // ERROR! Cannot borrow mutably
                // while borrowed immutably
```
```rust
let s = String::new();

// Spawn new thread with closure
let h = thread::spawn(move || { // move values into closure
    s.push("Hello, world");
    s
});

println!("{}", h.join().unwrap());
```

---

# Other cool features

Current:
* Auto-dereferencing
* type coercion

```rust
fn take_and_return(s: String) -> String { return s; };

fn takes_str(s: &str) {}

fn main() {
    let test = &String::new(); // reference to String

  //let test = take_and_return(*test);
    let test = take_and_return(test); // auto dereferences

    takes_str(test); // coerced to &str, a String 'slice'
}
```

* Helpful, passionate community
* Awesome libraries!

---

# The Future
### Rust 2018 edition: coming soon!

* Non-lexical lifetimes: No more struggling with lifetimes
* Async/await syntax: Based on zero-overhead Futures
* Web Assembly
    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>
